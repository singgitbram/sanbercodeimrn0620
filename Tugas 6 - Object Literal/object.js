// soal1
const util = require('util')
function arrayToObject(arr) {
    // Code di sini 
    let now = new Date()
    let thisYear = now.getFullYear()
    let obj = {}

    if (arr.length == 0) {
        console.log("")
    } else {
        for (let i = 0; i < arr.length; i++) {
            obj.firstName = arr[i][0]
            obj.lastName = arr[i][1]
            obj.gender = arr[i][2]
            arr[i][3] < thisYear ? obj.age = (thisYear - arr[i][3]) : obj.age = 'Invalid Birth Year'
            console.log(`${i + 1}. ${arr[i][0]} ${arr[i][1]}: `)
            console.log(obj)
        }
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

// soal2
function shoppingTime(memberId, money) {
    // you can only write your code here!
    let listBarang = [
        [
            'Sepatu Stacattu', 1500000
        ],
        [
            'Baju Zoro', 500000
        ],
        [
            'Baju H&N', 250000
        ],
        [
            'Sweater Uniklooh', 175000
        ],
        [
            'Casing Handphone', 50000
        ]
    ];

    if (!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    }
    let obj = {
        memberId: memberId,
        money: money,
        listPurchased: [],
        changeMoney: null
    }
    for (let i = 0; i < listBarang.length; i++) {
        if (money >= listBarang[i][1]) {
            obj.listPurchased.push(listBarang[i][0])
            money -= listBarang[i][1]
        }
    }
    obj.changeMoney = money
    return obj
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// soal3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    let hasil = []
    let obj = {}
    for (let i = 0; i < arrPenumpang.length; i++) {
        obj.penumpang = arrPenumpang[i][0]
        obj.naikDari = arrPenumpang[i][1]
        obj.tujuan = arrPenumpang[i][2]
        obj.bayar = (2000 * (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])))
        hasil.push(obj)
        obj = {}
    }
    return hasil
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]