// soal1
const golden = goldenFunction = () => console.log("this is golden!!")
// Driver Code
golden()

// soal2
const newFunction = literal = (firstName, lastName) => {
    return {
        fullName: () => console.log(`${firstName} ${lastName}`)
    }
}
//Driver Code 
newFunction("William", "Imoh").fullName()

// soal3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject
// Driver code
console.log(firstName, lastName, destination, occupation)

// soal4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// soal5
const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before) 