// soal1
function range(startNum, finishNum) {
    let hasil1 = []
    if (!startNum || !finishNum) {
        return -1
    } else {
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i++) {
                hasil1.push(i)
            }
        } else if (startNum > finishNum) {
            for (let j = startNum; j >= finishNum; j--) {
                hasil1.push(j)
            }
        }
    }
    return hasil1
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// soal2
console.log(`-------------------------------------------------------------`)
function rangeWithStep(startNum, finishNum, step) {
    let hasil2 = []
    if (!startNum || !finishNum) {
        return -1
    } else {
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i += step) {
                hasil2.push(i)
            }
        } else if (startNum > finishNum) {
            for (let j = startNum; j >= finishNum; j -= step) {
                hasil2.push(j)
            }
        }
    }
    return hasil2
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// soal3
console.log(`-------------------------------------------------------------`)

function sum(startNum, finishNum, step) {
    let trueStep = 0
    let hasil3 = []

    if (!step) {
        trueStep = 1
    } else {
        trueStep = step
    }

    if (!startNum && !finishNum && !step) {
        return 0
    } else if (!finishNum) {
        return startNum
    } else {
        if (startNum < finishNum) {
            for (let i = startNum; i <= finishNum; i += trueStep) {
                hasil3.push(i)
            }
        } else if (startNum > finishNum) {
            for (let j = startNum; j >= finishNum; j -= trueStep) {
                hasil3.push(j)
            }
        }
    }
    return hasil3.reduce((a, b) => a + b, 0)
}

// Code di sini
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// soal4
console.log(`-------------------------------------------------------------`)

function dataHandling(input) {
    let hasil4 = '';
    for (let i = 0; i < input.length; i++) {
        console.log(`Nomor ID: ${input[i][0]}`);
        console.log(`Nama Lengkap: ${input[i][1]}`);
        console.log(`TTL: ${input[i][2]} ${input[i][3]}`);
        console.log(`Hobi: ${input[i][4]} \n`);
    }
    return hasil4
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log(dataHandling(input));

// soal5
console.log(`-------------------------------------------------------------`)

function balikKata(kata) {
    let hasil5 = ''
    for (let i = kata.length - 1; i >= 0; i--) {
        hasil5 += kata[i]
    }
    return hasil5
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// soal6
console.log(`-------------------------------------------------------------`)

function dataHandling2(input) {

    input.splice(1, 2, 'Roman Alamsyah Elsharawy', 'Provinsi Bandar Lampung');
    input.splice(4, 1, 'Pria', 'SMA Internasional Metro');
    console.log(input)
    tanggalSplit = input[3].split('/');

    switch (tanggalSplit[1]) {
        case '01':
            console.log('Januari');
            break;
        case '02':
            console.log('Februari');
            break;
        case '03':
            console.log('Maret');
            break;
        case '04':
            console.log('April');
            break;
        case '05':
            console.log('Mei');
            break;
        case '06':
            console.log('Juni');
            break;
        case '07':
            console.log('Juli');
            break;
        case '08':
            console.log('Agustus');
            break;
        case '09':
            console.log('September');
            break;
        case '10':
            console.log('Oktober');
            break;
        case '11':
            console.log('November');
            break;
        case '12':
            console.log('Desember');
            break;
    }

    tanggalSort = tanggalSplit.sort(function (a, b) { return b - a });
    console.log(tanggalSort)
    console.log(input[3].split('/').join('-'))
    console.log(input[1].slice(0, 14))
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input)