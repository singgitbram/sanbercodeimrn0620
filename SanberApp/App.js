import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import 'react-native-gesture-handler';
import Tugas12 from './Tugas/Tugas12/App'
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import TodoApp from './Tugas/Tugas14/App'
import SkillScreen from './Tugas/Tugas14/SkillScreen'
import Tugas15 from './Tugas/Tugas15/index'
import TugasNavigation from './Tugas/TugasNavigation/index'
import Quiz3 from './Quiz3/index'
import Tugas16 from './Tugas/Tugas16/index'
import LatRedux1 from './Tugas/LatRedux1/index'

export default function App() {
  return (
      /* <Tugas12 />
      <LoginScreen />
      <AboutScreen />
      <TodoApp /> */
      // <SkillScreen />
      // <Tugas15 />
      // <TugasNavigation />
      // <Quiz3 />
      // <Tugas16 />
      <LatRedux1 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
